//
//  ContentView.swift
//  TraficLight_SwiftUI
//
//  Created by Дмитрий on 20.08.2023.
//

import SwiftUI

struct ContentView: View {
    
    @State private var redSliderValue = 120.0
    @State private var greenSliderValue = 200.0
    @State private var blueSliderValue = 140.0
    
    var body: some View {
        VStack(spacing: 20) {
            ColorView(
                red: redSliderValue,
                green: greenSliderValue,
                blue: blueSliderValue
            )
            ColorSlider(value: $redSliderValue, color: .red)
            ColorSlider(value: $greenSliderValue, color: .green)
            ColorSlider(value: $blueSliderValue, color: .blue)
            Spacer()
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

