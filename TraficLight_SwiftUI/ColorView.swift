//
//  ColorView.swift
//  TraficLight_SwiftUI
//
//  Created by Дмитрий on 20.08.2023.
//

import SwiftUI

struct ColorView: View {
    
    let red: Double
    let green: Double
    let blue: Double
    
    var body: some View {
        Rectangle()
            .frame( height: 160)
            .cornerRadius(20)
            .foregroundColor(Color(red: red / 255, green: green / 255, blue: blue/255))
    }
}

struct ColorView_Previews: PreviewProvider {
    static var previews: some View {
        ColorView(red: 100, green: 120, blue: 255)
    }
}
