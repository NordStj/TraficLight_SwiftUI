//
//  ColorSlider.swift
//  TraficLight_SwiftUI
//
//  Created by Дмитрий on 20.08.2023.
//

import SwiftUI

struct ColorSlider: View {
    
    @Binding var value: Double
    @State private var text = "color"
    @State private var showAlert = false
    
    let color: Color
    
    var body: some View {
        HStack {
            Text("\(lround(value))")
                .frame(width: 35)
                .multilineTextAlignment(.leading)
            Slider(value: $value, in: 0...255, step: 1)
                .accentColor(color)
            ColorTexfField(text: $text, action: checkTF )
                .alert("Wrong Format", isPresented: $showAlert, actions: {}) {
                    Text("Please enter current value")
                }
        }
        .onAppear(perform: {
            text = lround(value).formatted()
        })
        .onChange(of: value) { newValue in
            text = lround(newValue).formatted()
        }

        }
    
    private func updateText() {
          text = lround(value).formatted()
      }
    
    func checkTF() {
        if let numericValue = Double(text), (0...255).contains(numericValue) {
            value = numericValue
            return
        }
        text = "0"
        value = 0
        showAlert.toggle()
        
    }
}

struct ColorSlider_Previews: PreviewProvider {
    static var previews: some View {
        ColorSlider(value: .constant(100), color: .red)
    }
}
