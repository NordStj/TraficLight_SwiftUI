//
//  TraficLight_SwiftUIApp.swift
//  TraficLight_SwiftUI
//
//  Created by Дмитрий on 20.08.2023.
//

import SwiftUI

@main
struct TraficLight_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
