//
//  ColorTexfField.swift
//  TraficLight_SwiftUI
//
//  Created by Дмитрий on 20.08.2023.
//

import SwiftUI

struct ColorTexfField: View {
    

    @Binding var text: String
    
    let action: () -> Void
    
    var body: some View {
        TextField("", text: $text) { _ in
            withAnimation {
                action()
            }
        }
            .frame(width: 35)
            .padding(EdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4))
            .overlay(
                RoundedRectangle(cornerRadius: 2)
                    .stroke(lineWidth: 1)
            )
            .keyboardType(.decimalPad)
    }
}

struct ColorTexfField_Previews: PreviewProvider {
    static var previews: some View {
        ColorTexfField(text: .constant("100"), action: {})
    }
}
